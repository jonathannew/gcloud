#!/bin/bash

while read IPADDR
do
echo $IPADDR
ufw allow from $IPADDR to any port ${PORT:80} proto tcp
done < "${1:-/dev/stdin}"
